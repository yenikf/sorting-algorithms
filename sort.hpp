#include <algorithm>
#include <iterator>

namespace sort {

    template <typename Iterator>
    void bubble_sort (Iterator bg, Iterator en)
    {
        for ( ; bg != en; --en){
            auto maxval = bg;
            for ( ; (maxval + 1) != en; ++maxval){
                if (*maxval > *(maxval + 1)){
                    std::iter_swap (maxval, maxval + 1);
                }
            }
        }
        return;
    }

    template <typename Iterator>
    void selection_sort (Iterator pivot, Iterator en)
    {
        for ( ; pivot != en; ++pivot){
            auto m = std::min_element (pivot, en);
            std::iter_swap (pivot, m);
        }
        return;
    }

    template <typename Iterator>
    void insertion_sort (Iterator bg, Iterator en)
    {
        for (auto pivot = bg; pivot != en; ++pivot){
            for (auto x = pivot; (x != bg) && (*x < *(x - 1)); --x){
                std::iter_swap (x, x - 1);
            }
        }
        return;
    }

    template <typename Iterator>
    void insertion_sort_no_swap (Iterator bg, Iterator en)
    {
        for (auto pivot = bg; pivot != en; ++pivot){
            auto x = pivot;
            auto tmpval = *pivot;
            for ( ; (x != bg) && (tmpval < *(x - 1)); --x){
                *x = *(x - 1);
            }
            *x = tmpval;
        }
        return;
    }

    template <typename Iterator>
    void merge_sort (Iterator bg, Iterator en)
    {
        using val_type = typename std::iterator_traits<Iterator>::value_type;
        if (bg != (en - 1)){
            auto split = bg + (en - bg) / 2;
            std::vector<val_type> v1 {bg, split};
            std::vector<val_type> v2 {split, en};

            merge_sort (v1.begin(), v1.end());
            merge_sort (v2.begin(), v2.end());

            auto v1_it = v1.begin();
            auto v2_it = v2.begin();
            for ( ; bg != en; ++bg){
                if (v1_it == v1.end()){
                    *bg = *v2_it++;
                    continue;
                }
                if (v2_it == v2.end()){
                    *bg = *v1_it++;
                    continue;
                }
                if (*v2_it < *v1_it){
                    *bg = *v2_it++;
                } else {
                    *bg = *v1_it++;
                }
            }
        }
        return;
    }

}
