#include <chrono>
#include <ctime>        // time
#include <iostream>     // cout
#include <random>       // default_random_engine, uniform_int_distribution
#include <thread>       // thread
#include <vector>       // vector
#include "sort.hpp"

template <typename Iterator>
std::vector<typename std::iterator_traits<Iterator>::value_type>
merge (Iterator bg1, Iterator en1, Iterator bg2, Iterator en2)
{
    std::vector<typename std::iterator_traits<Iterator>::value_type> v;
    while (bg1 != en1 || bg2 != en2){
        if (bg1 == en1){
            v.push_back (*bg2++);
            continue;
        }
        if (bg2 == en2){
            v.push_back (*bg1++);
            continue;
        }
        if (*bg2 < *bg1){
            v.push_back (*bg2++);
        } else {
            v.push_back (*bg1++);
        }
    }
    return v;
}

template <typename C>
C get_random_container (size_t sz, int from = 0, int to = 100)
{
    static std::default_random_engine e (time (0));
    std::uniform_int_distribution<int> dist (from, to);

    C c;
    for (size_t x = 0; x < sz; ++x){
        c.push_back(dist(e));
    }
    return c;
}

template <typename C>
void print_content (C const& c)
{
    std::copy (c.cbegin (), c.cend (), std::ostream_iterator<typename C::value_type> (std::cout, "  "));
    std::cout << '\n';
    return;
}

void run()
{
    using namespace std;

    auto vec = get_random_container<vector<int>> (100, 0, 40000);

    cout << "generated: ";
//    print_content (vec);

    auto time_start = std::chrono::steady_clock::now();
//    sort::bubble_sort (vec.begin(), vec.end());
//    sort::selection_sort (vec.begin(), vec.end());
//    sort::insertion_sort (vec.begin(), vec.end());
//    sort::insertion_sort_no_swap (vec.begin(), vec.end());
//    sort::merge_sort (vec.begin(), vec.end());

    thread t1 (sort::selection_sort<vector<int>::iterator>, vec.begin(), vec.begin() + vec.size() / 4 );
    thread t2 (sort::selection_sort<vector<int>::iterator>, vec.begin() + vec.size() / 4, vec.begin() + vec.size() / 2 );
    thread t3 (sort::selection_sort<vector<int>::iterator>, vec.begin() + vec.size() / 2, vec.begin() + vec.size() * 3 / 4 );
    thread t4 (sort::selection_sort<vector<int>::iterator>, vec.begin() + vec.size() * 3 / 4, vec.begin() + vec.size() );
    t1.join();
    t2.join();
    t3.join();
    t4.join();
    auto vec_tmp1 = ::merge (vec.begin(), vec.begin() + vec.size() / 4, vec.begin() + vec.size() / 4, vec.begin() + vec.size() / 2);
    auto vec_tmp2 = ::merge (vec.begin() + vec.size() / 2, vec.begin() + vec.size() * 3 / 4, vec.begin() + vec.size() * 3 / 4, vec.begin() + vec.size());
    auto vec_sorted = ::merge (vec_tmp1.begin(), vec_tmp1.end(), vec_tmp2.begin(), vec_tmp2.end());

    auto time_stop = std::chrono::steady_clock::now();

    cout << "sorted: ";
    print_content (vec);
    cout << "time spent sorting: " << chrono::duration_cast<chrono::milliseconds>(time_stop - time_start).count() << " milliseconds.\n";

    return;
}

int main ()
{
    try {
        run ();
    }
    catch (std::exception& e){
        std::cerr << e.what ();
    }
    return 0;
}
